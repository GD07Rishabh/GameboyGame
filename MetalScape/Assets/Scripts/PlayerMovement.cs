using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]

public class PlayerMovement : MonoBehaviour
{
    private CustomInput input = null;
    private Vector2 moveVector = Vector2.zero;
    private CharacterController charController;

    [SerializeField]
    private float charSpeed = 5.0f;

    private void Awake()
    {
        input = new CustomInput();
        charController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        charController.Move(moveVector * Time.deltaTime * charSpeed);
    }


    private void OnEnable()
    {
        input.Enable();
        input.Player.Movement.performed += onMovementPerformed;
        input.Player.Movement.canceled += onMovementCancelled;
        input.Player.Jump.performed += onJump;
        input.Player.Action.performed += Action;
    }

    private void OnDisable()
    {
        input.Disable();
        input.Player.Movement.performed -= onMovementPerformed;
        input.Player.Movement.canceled -= onMovementCancelled;
    }


    void onMovementPerformed(InputAction.CallbackContext value)
    {
        Debug.Log(value.ReadValue<Vector2>().x +"  "+ value.ReadValue<Vector2>().y );
        moveVector = value.ReadValue<Vector2>();
    }

    void onMovementCancelled(InputAction.CallbackContext value)
    {
        moveVector = Vector2.zero;
    }

    void onJump(InputAction.CallbackContext value )
    {
        Debug.Log("Jump");
    }

    void Action(InputAction.CallbackContext value)
    {
        Debug.Log("Action");
    }




}
